package servlets;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import statics.StaticsData;

@WebServlet("/main/*")

public class ServletMain extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		int nextNumber = ThreadLocalRandom.current().nextInt(0, 1000000);
		while (StaticsData.mapUser.containsKey(nextNumber)) {
			nextNumber = ThreadLocalRandom.current().nextInt(0, 1000000);
		}
		StaticsData.mapUser.put(nextNumber, 0);
		request.setAttribute("number", nextNumber);

		System.out.println("Number of next user:" + nextNumber);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/main.jsp");
		dispatcher.forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		int numb = Integer.parseInt(request.getPathInfo().substring(1));
		int rangeNumber = StaticsData.mapUser.get(numb) + 1;
		if (rangeNumber>9){rangeNumber=0;}
	StaticsData.mapUser.replace(numb, rangeNumber);
		request.setAttribute("personalNumber", rangeNumber);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/sendData.jsp");
		dispatcher.forward(request, response);
	}
}
